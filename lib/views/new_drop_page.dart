import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../new_car_repository.dart';
import '../new_country/new_drop_bloc.dart';
import '../new_country/new_drop_event.dart';

class NewDropPage extends StatefulWidget {
  const NewDropPage({Key? key}) : super(key: key);

  @override
  _NewDropPageState createState() => _NewDropPageState();
}

late TextEditingController nameController, numberController;

class _NewDropPageState extends State<NewDropPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Flutter dropdown choose')),
      body: BlocProvider(
        create: (_) => NewDropBloc(
          newDropRepository: context.read<NewDropRepository>(),
        )..add(const NewDropLoaded()),
        child: NewCarForm(),
      ),
      floatingActionButton: BlocProvider(
        create: (context) => NewDropBloc(
          newDropRepository: context.read<NewDropRepository>(),
        ),
        child: _FormSubmitButton(),
      ),
      floatingActionButtonLocation:
          FloatingActionButtonLocation.miniCenterDocked,
    );
  }
}

class NewCarForm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            TextFormField(

            ),
            Row(
              children: [
                Flexible(
                    flex: 1,
                    child: TextFormField(
                      initialValue: '+998',
                      readOnly: true,
                    )),
                SizedBox(
                  width: 20,
                ),
                Flexible(flex: 3, child: TextFormField()),
              ],
            ),
            const _ProvinceDropdownButton(),
            const _CityDropdownButton(),
            TextFormField(),
            TextFormField(),
            TextFormField(),
          ],
        ),
      ),
    );
  }
}

class _ProvinceDropdownButton extends StatelessWidget {
  const _ProvinceDropdownButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final provinces =
        context.select((NewDropBloc bloc) => bloc.state.provinces);
    final province = context.select((NewDropBloc bloc) => bloc.state.province);
    return Material(
      child: DropdownButton<String>(
        isExpanded: true,
        items: provinces.isNotEmpty
            ? provinces.map((province) {
                return DropdownMenuItem(value: province, child: Text(province));
              }).toList()
            : const [],
        value: province,
        hint: const Text('Select a Province'),
        onChanged: (province) {
          context
              .read<NewDropBloc>()
              .add(NewDropProvinceChanged(province: province));
        },
      ),
    );
  }
}

class _CityDropdownButton extends StatelessWidget {
  const _CityDropdownButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final cities = context.select((NewDropBloc bloc) => bloc.state.cities);
    final city = context.select((NewDropBloc bloc) => bloc.state.city);
    return Material(
      child: DropdownButton<String>(
        isExpanded: true,
        items: cities.isNotEmpty
            ? cities.map((city) {
                return DropdownMenuItem(value: city, child: Text(city));
              }).toList()
            : const [],
        value: city,
        hint: const Text('Select a City'),
        onChanged: (city) {
          context.read<NewDropBloc>().add(NewDropCityChanged(city: city));
        },
      ),
    );
  }
}

class _FormSubmitButton extends StatelessWidget {
  const _FormSubmitButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final state = context.watch<NewDropBloc>().state;

    void _onFormSubmitted() {
      ScaffoldMessenger.of(context)
        ..hideCurrentSnackBar()
        ..showSnackBar(
          SnackBar(
            content: Text('Submitted ${state.province} ${state.city}'),
          ),
        );
    }

    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(16),
      child: ElevatedButton(
        onPressed: state.isComplete ? _onFormSubmitted : null,
        child: const Text('Submit'),
      ),
    );
  }
}
