import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dropdownchoose/new_car_repository.dart';
import 'package:flutter_dropdownchoose/views/new_drop_page.dart';

void main() => runApp(MyApp(newDropRepository: NewDropRepository()));

class MyApp extends StatelessWidget {
  const MyApp({Key? key, required this.newDropRepository}) : super(key: key);

  final NewDropRepository newDropRepository;

  @override
  Widget build(BuildContext context) {
    return RepositoryProvider.value(
      value: newDropRepository,
      child: MaterialApp(home: NewDropPage()),
    );
  }
}