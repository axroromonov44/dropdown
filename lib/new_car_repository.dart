const _delay = Duration(microseconds: 200);

Future<void> wait() => Future.delayed(_delay);

class NewDropRepository {
  Future<List<String>> fetchProvinces() async {
    await wait();
    return [
      'Tashkent',
      'Kashkadarya',
    ];
  }

  Future<List<String>> fetchCities({String? province}) async {
    await wait();
    switch (province) {
      case 'Tashkent':
        return ['Chilanzar', 'Yunusabad'];
      case 'Kashkadarya':
        return ['Kitab', 'Shahrisabz'];
      default:
        return [];
    }
  }
}
