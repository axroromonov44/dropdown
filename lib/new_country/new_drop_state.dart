part of 'new_drop_event.dart';

@immutable
 class NewDropState extends Equatable {
  const NewDropState._({
    this.provinces = const <String>[],
    this.province,
    this.cities = const <String>[],
    this.city,
  });


  const NewDropState.initial() : this._();

  const NewDropState.provincesLoadInProgress() : this._();


  const NewDropState.provincesLoadSuccess({required List<String> provinces})
  : this._(provinces : provinces);

  const NewDropState.citiesLoadInProgress({required List<String> provinces,
  
  String? province}) : this ._(provinces : provinces , province : province);

  const NewDropState.citiesLoadSuccess({
    required List<String> provinces,
    required String? province,
    required List<String> cities,
  }) : this._(provinces: provinces, province: province, cities: cities);



  NewDropState copyWith({
    List<String>? provinces,
    String? province,
    List<String>? cities,
    String? city,

  }) {
    return NewDropState._(
      provinces: provinces ?? this.provinces,
      province: province ?? this.province,
      cities: cities ?? this.cities,
      city: city ?? this.city,
    );
  }


  bool get isComplete => province != null && city != null;


  @override
  // TODO: implement props
  List<Object?> get props => [provinces, province , cities , city];

  final List<String> provinces;
  final String? province;

  final List<String> cities;
  final String? city;

}
