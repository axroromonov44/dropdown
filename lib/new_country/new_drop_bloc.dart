import 'package:bloc/bloc.dart';
import 'package:flutter_dropdownchoose/new_car_repository.dart';
import 'package:flutter_dropdownchoose/new_country/new_drop_event.dart';

class NewDropBloc extends Bloc<NewDropEvent , NewDropState> {
  NewDropBloc({required NewDropRepository newDropRepository})
  : _newDropRepository = newDropRepository , super (const NewDropState.initial());

  final NewDropRepository _newDropRepository;

  @override
  Stream<NewDropState> mapEventToState(NewDropEvent event) async* {
    if (event is NewDropLoaded) {
      yield* _mapNewDropFormLoadedToState();
    } else if (event is NewDropProvinceChanged) {
      yield* _mapNewDropProvincesChangedToState(event,state);
    }    else if (event is NewDropCityChanged) {
      yield _mapNewDropChangedToState(event);
    }
    // else if (event is NewCarYearChanged) {
    //   yield _mapNewCarYearChangedToState(event);
    // }

  }

  Stream<NewDropState> _mapNewDropFormLoadedToState() async* {
    yield const NewDropState.provincesLoadInProgress();
    final provinces = await _newDropRepository.fetchProvinces();
    yield NewDropState.provincesLoadSuccess(provinces: provinces);
  }

  Stream<NewDropState> _mapNewDropProvincesChangedToState(
      NewDropProvinceChanged event,
      NewDropState state,
      ) async* {
    yield NewDropState.citiesLoadInProgress(
      provinces: state.provinces,
      province: event.province,
    );
    final cities = await _newDropRepository.fetchCities(province: event.province);
    yield NewDropState.citiesLoadSuccess(
      provinces: state.provinces,
      province: event.province,
      cities: cities,
    );
  }

  NewDropState _mapNewDropChangedToState(NewDropCityChanged event) {
    return state.copyWith(city: event.city);
  }

}