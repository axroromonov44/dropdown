import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'new_drop_state.dart';

abstract class NewDropEvent extends Equatable {
  const NewDropEvent();

  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class NewDropLoaded extends NewDropEvent {
  const NewDropLoaded();

}
class NewDropProvinceChanged extends NewDropEvent {
  const NewDropProvinceChanged({this.province});

  final String? province;

  @override
  List<Object?> get props => [province];
}


class NewDropCityChanged extends NewDropEvent {
  const NewDropCityChanged({this.city});

  final String? city;

  @override
  List<Object?> get props => [city];
}